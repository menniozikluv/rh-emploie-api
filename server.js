const express = require('express');
const mysql = require('mysql');
const cors = require('cors');


const app = express();
const port = 3000;

// Configuration de la connexion MySQL
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'rh'
});

app.use(cors());


app.get('/api/rh/categorie/:cat', (req, res) => {
    const cat = req.params.cat;
    const query = `SELECT * FROM offre WHERE categorie LIKE '%${cat}%'`;
  
    connection.query(query, (error, results) => {
      if (error) throw error;
      res.json(results);
    });
});

app.get('/api/rh/distinct/lieu', (req, res) => {
  const lieu = req.params.lieu;
  const query = `SELECT DISTINCT lieu FROM offre`;

  connection.query(query, (error, results) => {
    if (error) throw error;
    res.json(results);
  });
});

app.get('/api/rh/distinct/contrat', (req, res) => {
  const lieu = req.params.lieu;
  const query = `SELECT DISTINCT contrat FROM offre`;

  connection.query(query, (error, results) => {
    if (error) throw error;
    res.json(results);
  });
});

app.get('/api/rh/contrat/:contrat', (req, res) => {
  const contrat = req.params.contrat;
  const query = `SELECT * FROM offre WHERE contrat LIKE  '%${contrat}%'`;

  connection.query(query, (error, results) => {
    if (error) throw error;
    res.json(results);
  });
}); 

// Votre code Express.js ici
app.get('/api/rh', (req, res) => {
    connection.query('SELECT * FROM offre', (error, results) => {
      if (error) throw error;
      res.json(results);
    });
});

app.get('/api/rh/lieu/:searchQuery', (req, res) => {
    const searchQuery = req.params.searchQuery;
    const query = `SELECT * FROM offre WHERE lieu LIKE '%${searchQuery}%'`;
  
    connection.query(query, (error, results) => {
      if (error) throw error;
      res.json(results);
    });
});

app.get('/api/rh/titre/:searchQuery', (req, res) => {
    const searchQuery = req.params.searchQuery;
    const query = `SELECT * FROM offre WHERE titre LIKE '%${searchQuery}%'`;
  
    connection.query(query, (error, results) => {
      if (error) throw error;
      res.json(results);
    });
});

app.get('/api/rh/:searchLocation/:searchQuery', (req, res) => {
    const searchLocation = req.params.searchLocation;
    const searchQuery = req.params.searchQuery;
    const query = `SELECT * FROM offre WHERE lieu LIKE  '%${searchLocation}%' AND titre LIKE '%${searchQuery}%'`;
  
    connection.query(query, (error, results) => {
      if (error) throw error;
      res.json(results);
    });
});  

app.get('/api/rh/lieu/:lieu', (req, res) => {
  const lieu = req.params.lieu;
  const query = `SELECT * FROM offre WHERE lieu LIKE  '%${lieu}%'`;

  connection.query(query, (error, results) => {
    if (error) throw error;
    res.json(results);
  });
});

// Démarrage du serveur
app.listen(port, () => {
  console.log(`Serveur Express.js démarré sur le port ${port}`);
});
